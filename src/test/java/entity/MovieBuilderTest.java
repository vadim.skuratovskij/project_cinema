package entity;

import cinema.entities.*;
import cinema.exceptions.InvalidFieldException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Set;

public class MovieBuilderTest {

    @DataProvider(name = "data-provider-correct-date-movies")
    public Object[][] dataProviderMethodCorrectMovies() {
        return new Object[][] {
                {
                        "title1"
                        , Duration.ofMinutes(12)
                        , Set.of(Genre.FANTASY, Genre.HORROR)
                        , 1.25f
                }
                , {
                        "title 2"
                        , Duration.ofMinutes(123)
                        , Set.of()
                        , 7.0f
                   }
                , {
                        "3 title"
                        , Duration.ofMinutes(35)
                        , Set.of(Genre.DRAMA)
                        , 7.0f
                   }
                , {
                        "4 TITLE 4"
                        , Duration.ofMinutes(33)
                        , Set.of(Genre.COMEDY, Genre.HORROR)
                        , 3.4f
                  }
                , {
                        "title 5 title"
                        , Duration.ofMinutes(66)
                        , Set.of(Genre.COMEDY, Genre.HORROR, Genre.FANTASY)
                        , 5.55f
                  }
        };
    }

    @DataProvider(name = "data-provider-incorrect-date-movies")
    public Object[][] dataProviderMethodInCorrectMovies() {
        return new Object[][] {
                {
                        "title1"
                        , Duration.ofMinutes(12)
                        , Set.of(Genre.FANTASY, Genre.HORROR)
                        , -1f
                }
                , {
                        "title 2"
                        , Duration.ofMinutes(123)
                        , Set.of()
                        , 11.0f
                  }
                , {
                        "3 title"
                        , null
                        , Set.of(Genre.DRAMA)
                        , 7.0f
                  }
                , {
                        "     "
                        , Duration.ofMinutes(33)
                        , Set.of(Genre.COMEDY, Genre.HORROR)
                        , 3.4f
                  }
                , {
                        null
                        , Duration.ofMinutes(66)
                        , Set.of(Genre.COMEDY, Genre.HORROR, Genre.FANTASY)
                        , 5.55f
                  }
        };
    }

    @Test(dataProvider = "data-provider-correct-date-movies")
    public void testCorrectMovieBuilder(String title, Duration duration, Set<Genre> genres, float rating) {
        Movie movie = new MovieBuilder()
                .setTitle(title)
                .setDuration(duration)
                .setGenres(genres)
                .setRating(rating)
                .build();
        Assert.assertNotNull(movie);
    }

    @Test(dataProvider = "data-provider-incorrect-date-movies"
            , expectedExceptions = {InvalidFieldException.class}
            , expectedExceptionsMessageRegExp = ".*((title cannot be blank)|(duration cannot be null)|(rating cannot be less than 0)|(rating cannot be bigger than 10)).*")
    public void testIncorrectMovieBuilder(String title, Duration duration, Set<Genre> genres, float rating) {
        Movie movie = new MovieBuilder()
                .setTitle(title)
                .setDuration(duration)
                .setGenres(genres)
                .setRating(rating)
                .build();
    }

}
