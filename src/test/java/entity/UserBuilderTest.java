package entity;

import cinema.entities.User;
import cinema.entities.UserBuilder;
import cinema.exceptions.InvalidFieldException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UserBuilderTest {

    @DataProvider(name = "data-provider-correct-date-users")
    public Object[][] dataProviderMethodCorrectUsers() {
        return new Object[][] {
                { "vadim", "+38 (095)8827299" }
                , { "person1", "+38 (095) 12-17-123" }
                , { "person2", "0952312477" }
                , { "person3", "(095) 2312477" }
                , { "person4", null }
        };
    }

    @DataProvider(name = "data-provider-incorrect-date-users")
    public Object[][] dataProviderMethodInCorrectUsers() {
        return new Object[][] {
                { null, "+38 (095)8827299" }
                , { "person1", "+qwe" }
                , { "person2", "0952312477123" }
                , { "     ", "(095) 2312477" }
                , { "person4", "   " }
        };
    }

    @Test(dataProvider = "data-provider-correct-date-users")
    public void testCorrectUserBuilder(String name, String phone) {
        User user = new UserBuilder()
                .setName(name)
                .setPhone(phone)
                .build();
        Assert.assertNotNull(user);
    }

    @Test(dataProvider = "data-provider-incorrect-date-users"
            , expectedExceptions = {InvalidFieldException.class}
            , expectedExceptionsMessageRegExp = ".*((title cannot be blank)|(phone has wrong format)).*")
    public void testIncorrectUserBuilder(String name, String phone) {
        User user = new UserBuilder()
                .setName(name)
                .setPhone(phone)
                .build();
    }
}
