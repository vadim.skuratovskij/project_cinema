package entity;

import cinema.entities.*;
import cinema.exceptions.InvalidFieldException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Set;

public class TicketBuilderTest {

    @DataProvider(name = "data-provider-correct-date-tickets")
    public Object[][] dataProviderMethodCorrectTickets() {
        User user = new UserBuilder()
                .setName("vadim")
                .setPhone("+38 (095) 88-27-299")
                .build();
        Movie movie = new MovieBuilder()
                .setTitle("movie1")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        return new Object[][] {
                { movie, user, LocalDateTime.now(), 111.1f}
                , { movie, user, LocalDateTime.of(2020, 10,10,10,10), 111.1f }
                , { movie, user, LocalDateTime.of(2019, 1,12,5,3), 256f }
                , { movie, user, LocalDateTime.of(2000, 2,18,6,1), 0f }
                , { movie, user, null, 12.2f }
        };
    }

    @DataProvider(name = "data-provider-incorrect-date-tickets")
    public Object[][] dataProviderMethodIncorrectTickets() {
        User user = new UserBuilder()
                .setName("vadim")
                .setPhone("+38 (095) 88-27-299")
                .build();
        Movie movie = new MovieBuilder()
                .setTitle("movie1")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        return new Object[][] {
                { null, user, LocalDateTime.now(), 111.1f}
                , { movie, null, LocalDateTime.of(2020, 10,10,10,10), 111.1f }
                , { movie, user, LocalDateTime.of(2023, 1,12,5,3), 256f }
                , { movie, user, LocalDateTime.of(2000, 2,18,6,1), -1 }
                , { movie, user, LocalDateTime.of(2018, 1,12,5,3), -12.2f }
        };
    }

    @Test(dataProvider = "data-provider-correct-date-tickets")
    public void testCorrectTicketBuilder(Movie movie, User user, LocalDateTime date, float price) {
        Ticket ticket = new TicketBuilder()
                .setMovie(movie)
                .setUser(user)
                .setDate(date)
                .setPrice(price)
                .build();
        Assert.assertNotNull(ticket);
    }

    @Test(dataProvider = "data-provider-incorrect-date-tickets"
            , expectedExceptions = {InvalidFieldException.class}
            , expectedExceptionsMessageRegExp = ".*((movie cannot be null)|(user cannot be null)|(the date must be past or present)|(price cannot be null)|(price must be positive or zero)).*")
    public void testIncorrectTicketBuilder(Movie movie, User user, LocalDateTime date, float price) {
        Ticket ticket = new TicketBuilder()
                .setMovie(movie)
                .setUser(user)
                .setDate(date)
                .setPrice(price)
                .build();
    }

}
