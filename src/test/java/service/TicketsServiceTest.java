package service;

import cinema.entities.*;
import cinema.repositories.db_repositories_impl.DbMovieRepositoryImpl;
import cinema.repositories.db_repositories_impl.DbTicketRepositoryImpl;
import cinema.repositories.db_repositories_impl.DbUserRepositoryImpl;
import cinema.repositories.interfaces.MovieRepository;
import cinema.repositories.interfaces.TicketRepository;
import cinema.repositories.interfaces.UserRepository;
import cinema.services.services_impl.TicketServiceImpl;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class TicketsServiceTest {
    private TicketServiceImpl ticketServiceImpl;
    private TicketRepository<Ticket, UUID> ticketRepository;
    private List<Ticket> createdTickets;

    private Connection connection;
    private File tempFile;

    private UserRepository<User, UUID> userRepository;
    private MovieRepository<Movie, UUID> movieRepository;
    private List<Movie> createdMovie;
    private List<User> createdUser;

    @BeforeClass
    public void beforeClass() throws IOException, SQLException, ClassNotFoundException {
        Properties props = new Properties();
        String dbSettingsPropertyFile = "src\\test\\resources\\jdbc.properties";
        try(FileReader fReader = new FileReader(dbSettingsPropertyFile)) {
            props.load(fReader);
            String dbConnUrl = props.getProperty("db.conn.url");
            String dbUserName = props.getProperty("db.username");
            String dbPassword = props.getProperty("db.password");
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);
        }
        connection.setAutoCommit(false);

        tempFile = File.createTempFile("TempTickets", ".json");
        tempFile.deleteOnExit();

        User user1 = new UserBuilder()
                .setName("vadim")
                .setPhone("+38 (095) 88-27-299")
                .build();
        User user2 = new UserBuilder()
                .setName("person")
                .setPhone("+38 (095) 23-12-477")
                .build();
        User user3 = new UserBuilder()
                .setName("new person")
                .setPhone("+38 (095) 12-34-567")
                .build();
        User user4 = new UserBuilder()
                .setName("person1")
                .setPhone("+38 (095) 12-56-124")
                .build();

        Movie movie1 = new MovieBuilder()
                .setTitle("movie1")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        Movie movie2 = new MovieBuilder()
                .setTitle("movie2")
                .setDuration(Duration.ofMinutes(56))
                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
                .setRating(1.1f)
                .build();
        Movie movie3 = new MovieBuilder()
                .setTitle("movie3")
                .setDuration(Duration.ofMinutes(11))
                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
                .setRating(2f)
                .build();
        Movie movie4 = new MovieBuilder()
                .setTitle("movie4")
                .setDuration(Duration.ofMinutes(125))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(6.6f)
                .build();


        Ticket ticket1 = new TicketBuilder()
                .setUser(user1)
                .setMovie(movie1)
                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
                .setPrice(101.1f)
                .build();
        Ticket ticket2 = new TicketBuilder()
                .setUser(user2)
                .setMovie(movie1)
                .setPrice(321.6f)
                .setDate(LocalDateTime.of(2021, 5, 11, 11,44, 0))
                .build();
        Ticket ticket3 = new TicketBuilder()
                .setUser(user1)
                .setMovie(movie3)
                .setPrice(156.1f)
                .setDate(LocalDateTime.of(2021, 6, 2, 3,44, 0))
                .build();

        Ticket ticket4 = new TicketBuilder()
                .setUser(user4)
                .setMovie(movie2)
                .setPrice(67.4f)
                .build();
        Ticket ticket5 = new TicketBuilder()
                .setUser(user3)
                .setMovie(movie3)
                .setPrice(13.4f)
                .setDate(LocalDateTime.now())
                .build();
        Ticket ticket6 = new TicketBuilder()
                .setUser(user4)
                .setMovie(movie4)
                .setPrice(19.2f)
                .setDate(LocalDateTime.now())
                .build();
        Ticket ticket7 = new TicketBuilder()
                .setUser(user1)
                .setMovie(movie1)
                .setPrice(10.1f)
                .setDate(LocalDateTime.now())
                .build();

        Ticket ticket8 = new TicketBuilder()
                .setUser(user1)
                .setMovie(movie3)
                .setPrice(10.1f)
                .setDate(LocalDateTime.of(2019, 10,10,10,10))
                .build();

        createdUser = new ArrayList<>();
        createdUser.add(user1);
        createdUser.add(user2);
        createdUser.add(user3);
        createdUser.add(user4);

        createdMovie = new ArrayList<>();
        createdMovie.add(movie1);
        createdMovie.add(movie2);
        createdMovie.add(movie3);
        createdMovie.add(movie4);

        createdTickets = new ArrayList<>();
        createdTickets.add(ticket1);
        createdTickets.add(ticket2);
        createdTickets.add(ticket3);
        createdTickets.add(ticket4);
        createdTickets.add(ticket5);
        createdTickets.add(ticket6);
        createdTickets.add(ticket7);
        createdTickets.add(ticket8);
    }

//    @BeforeMethod
//    public void beforeMethod() throws IOException {
//        ticketRepository =  new JsonTicketRepositoryImpl(tempFile.getAbsolutePath());
//        ticketServiceImpl = new TicketServiceImpl(ticketRepository);
//    }
//
//    @AfterMethod
//    public void afterMethod() {
//        tempFile.delete();
//    }

    @BeforeMethod
    public void beforeMethodDb() throws SQLException {
        ticketRepository = new DbTicketRepositoryImpl(connection);
        userRepository = new DbUserRepositoryImpl(connection);
        movieRepository = new DbMovieRepositoryImpl(connection);
        ticketRepository.deleteAll();
        userRepository.deleteAll();
        movieRepository.deleteAll();
        Arrays.stream(createdUser.toArray())
                .forEach(user -> {
                    try {
                        userRepository.create((User) user);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
        Arrays.stream(createdMovie.toArray())
                .forEach(movie -> {
                    try {
                        movieRepository.create((Movie) movie);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
        Arrays.stream(createdTickets.toArray())
                .forEach(ticket -> {
                    try {
                        ticketRepository.create((Ticket) ticket);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
        ticketServiceImpl = new TicketServiceImpl(ticketRepository);
    }
//
//    @DataProvider(name = "data-provider-correct-tickets")
//    public Object[][] dataProviderMethodCorrectTickets() {
//        return new Object[][] { { createdTickets.get(0) }, { createdTickets.get(1) }, { createdTickets.get(2) }, { createdTickets.get(3) } };
//    }
//
//    @DataProvider(name = "data-provider-correct-tickets-double-different")
//    public Object[][] dataProviderMethodCorrectTicketsDoubleDifferent() {
//        return new Object[][] {
//                { createdTickets.get(0), createdTickets.get(1) }
//                , { createdTickets.get(2), createdTickets.get(3) }
//                , { createdTickets.get(0), createdTickets.get(3) }
//        };
//    }
//
//    @DataProvider(name = "data-provider-incorrect-tickets-double-same")
//    public Object[][] dataProviderMethodIncorrectUsersDoubleSame() {
//        User user1 = new UserBuilder()
//                .setName("vadim")
//                .setPhone("+38 (095) 88-27-299")
//                .build();
//        User user2 = new UserBuilder()
//                .setName("person")
//                .setPhone("+38 (095) 23-12-477")
//                .build();
//
//        Movie movie1 = new MovieBuilder()
//                .setTitle("movie1")
//                .setDuration(Duration.ofMinutes(34))
//                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
//                .setRating(1.1f)
//                .build();
//        Movie movie2 = new MovieBuilder()
//                .setTitle("movie2")
//                .setDuration(Duration.ofMinutes(56))
//                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
//                .setRating(1.1f)
//                .build();
//
//        Ticket ticket1 = new TicketBuilder()
//                .setUser(user1)
//                .setMovie(movie1)
//                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
//                .setPrice(101.1f)
//                .build();
//        Ticket ticket2 = new TicketBuilder()
//                .setUser(user1)
//                .setMovie(movie1)
//                .setPrice(321.6f)
//                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
//                .build();
//        Ticket ticket3 = new TicketBuilder()
//                .setUser(user2)
//                .setMovie(movie2)
//                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
//                .setPrice(156.1f)
//                .build();
//        Ticket ticket4 = new TicketBuilder()
//                .setUser(user2)
//                .setMovie(movie2)
//                .setPrice(123.6f)
//                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
//                .build();
//        return new Object[][] {{ ticket1, ticket2 }, { ticket3, ticket4 }};
//    }
//
//    @DataProvider(name = "data-provider-correct-tickets-varargs")
//    public Object[][] dataProviderMethodCorrectUsersVarargs() {
//        return new Object[][] {
//                { new Ticket[0] }
//                , { createdTickets.get(0) }
//                , { createdTickets.get(0), createdTickets.get(1) }
//                , { createdTickets.get(0), createdTickets.get(1), createdTickets.get(2), createdTickets.get(3) }
//        };
//    }
//
//    @DataProvider(name = "data-provider-correct-tickets-varargs-with-date")
//    public Object[][] dataProviderMethodCorrectUsersVarargsWithDate() {
//        return new Object[][] {
//                { LocalDateTime.now().minusDays(3), new Ticket[0]}
//                , {LocalDateTime.now().minusDays(2), createdTickets.get(0)}
//                , {LocalDateTime.now().minusDays(1), createdTickets.get(0), createdTickets.get(1)}
//                , {LocalDateTime.now(), createdTickets.get(0), createdTickets.get(1), createdTickets.get(2), createdTickets.get(3)}
//        };
//    }
//
//    @DataProvider(name = "data-provider-for-getMoviesByCurrentDateOfDay")
//    public Object[][] dataProviderMethodForGetMoviesByCurrentDateOfDay() {
//        return new Object[][]{
//                { List.of(createdTickets.get(2).getMovie(), createdTickets.get(3).getMovie()), createdTickets}
//        };
//    }
//
//    @DataProvider(name = "data-provider-for-getUsersByDateOfDay")
//    public Object[][] dataProviderMethodForGetUsersByDateOfDay() {
//        return new Object[][] {
//                { List.of(), LocalDateTime.now().plusDays(1), createdTickets}
//                , { List.of(createdTickets.get(2).getUser(), createdTickets.get(3).getUser()), LocalDateTime.now(), createdTickets}
//        };
//    }
//
//    @DataProvider(name = "data-provider-for-getSumByDateOfMonth")
//    public Object[][] dataProviderMethodForGetSumByDateOfMonth() {
//        return new Object[][] {
//                { 279.7f, LocalDateTime.now(), createdTickets}
//        };
//    }
//
//    @DataProvider(name = "data-provider-for-getMoviesIfCountOfTicketsByMovieLessThenNumber")
//    public Object[][] dataProviderMethodForGetMoviesIfCountOfTicketsByMovieLessThenNumber() {
//        return new Object[][] {
//                { List.of(createdTickets.get(0).getMovie(), createdTickets.get(1).getMovie())
//                        , List.of(createdTickets.get(0), createdTickets.get(1)) }
//        };
//    }
//
//    @DataProvider(name = "data-provider-for-getSortedMoviesByCountOfTickets")
//    public Object[][] dataProviderMethodForGetSortedMoviesByCountOfTickets() {
//        return new Object[][] {
//                { List.of(createdTickets.get(0).getMovie(), createdTickets.get(1).getMovie())
//                        , List.of(createdTickets.get(0), createdTickets.get(1)) }
//        };
//    }

    @Test
    public void testCorrectCreateNewTickets() throws SQLException {
        User user = createdUser.get(0);
        Movie movie = createdMovie.get(0);
        Ticket ticket = new TicketBuilder()
                .setUser(user)
                .setMovie(movie)
                .setDate(LocalDateTime.of(2021, 4, 12, 8,44, 0))
                .setPrice(103.1f)
                .build();

        ticketServiceImpl.create(ticket);
        Assert.assertTrue(ticketRepository.isContains(ticket));
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Ticket with these settings already exists.*")
    public void testIncorrectCreateNewTickets() {
        User user = createdUser.get(0);
        Movie movie = createdMovie.get(0);
        Ticket ticket = new TicketBuilder()
                .setUser(user)
                .setMovie(movie)
                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
                .setPrice(101.1f)
                .build();
        ticketServiceImpl.create(ticket);
    }

    @Test
    public void testGetAllTickets() {
        List<Ticket> ticketList = ticketServiceImpl.getAll();
        Assert.assertEquals(ticketList, createdTickets);
    }

    @Test
    public void testCorrectGetTicketById() {
        Ticket foundTicket = createdTickets.get(0);
        Assert.assertEquals(foundTicket, ticketServiceImpl.getById(foundTicket.getId()));
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Ticket with this id doesn't exist.*")
    public void testIncorrectGetTicketById() {
        ticketServiceImpl.getById(UUID.randomUUID());
    }

    @Test
    public void testIsExistsTicketById() {
        Ticket foundTicket = createdTickets.get(0);
        Assert.assertTrue(ticketServiceImpl.isExistsById(foundTicket.getId()));
    }

    @Test
    public void testIsNotExistsTicketsById() {
        Assert.assertFalse(ticketServiceImpl.isExistsById(UUID.randomUUID()));
    }

    @Test
    public void testIsContainsTicket() {
        Ticket foundTicket = createdTickets.get(0);
        Assert.assertTrue(ticketServiceImpl.isContains(foundTicket));
    }

    @Test
    public void testIsNotContainsTicket(){
        Ticket foundTicket = new Ticket(createdTickets.get(0));
        foundTicket.setDate(LocalDateTime.of(2020,10,10,10,10));
        Assert.assertFalse(ticketServiceImpl.isContains(foundTicket));
    }

    @Test
    public void testCorrectUpdateTicket() throws SQLException {
        Ticket ticketFromService = createdTickets.get(0);
        Ticket ticketNew = new TicketBuilder()
                .setUser(createdUser.get(1))
                .setMovie(createdMovie.get(1))
                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
                .setPrice(101.1f)
                .build();
        ticketNew.setId(ticketFromService.getId());
        ticketNew.setUser(ticketFromService.getUser());
        ticketNew.setMovie(ticketFromService.getMovie());
        ticketNew.setDate(ticketFromService.getDate());
        ticketServiceImpl.update(ticketNew);
        Assert.assertEquals(ticketRepository.getById(ticketFromService.getId()), ticketNew);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Ticket with these settings already exists.*")
    public void testIncorrectUpdateAllFieldsTicket() throws SQLException {
        Ticket ticketFromService = createdTickets.get(0);
        Ticket ticketNew = new Ticket(createdTickets.get(1));
        ticketNew.setUser(ticketFromService.getUser());
        ticketNew.setMovie(ticketFromService.getMovie());
        ticketNew.setDate(ticketFromService.getDate());
        ticketServiceImpl.update(ticketNew);
        Assert.assertEquals(ticketRepository.getById(ticketFromService.getId()), ticketNew);
    }

    @Test
    public void testCorrectDeleteTicketById() throws SQLException, ClassNotFoundException {
        Ticket foundTicket = createdTickets.get(0);
        ticketRepository.deleteById(foundTicket.getId());
        Assert.assertFalse(ticketRepository.isContains(foundTicket));
    }

    @Test
    public void testCorrectDeleteTicketByMovie() throws SQLException {
        Ticket foundTicketForDelete1 = createdTickets.get(0);
        Ticket foundTicketForDelete2 = createdTickets.get(1);
        Ticket foundTicketForNoDelete = createdTickets.get(2);
        ticketServiceImpl.deleteByMovie(foundTicketForDelete1.getMovie());
        Assert.assertTrue(!ticketRepository.isContains(foundTicketForDelete1)
                && !ticketRepository.isContains(foundTicketForDelete2)
                && ticketRepository.isContains(foundTicketForNoDelete));
    }

    @Test
    public void testCorrectDeleteTicketByUser() throws SQLException {
        Ticket foundTicketForDelete1 = createdTickets.get(0);
        Ticket foundTicketForDelete2 = createdTickets.get(2);
        Ticket foundTicketForNoDelete = createdTickets.get(1);
        ticketServiceImpl.deleteByUser(foundTicketForDelete1.getUser());
        Assert.assertTrue(!ticketRepository.isContains(foundTicketForDelete1)
                && !ticketRepository.isContains(foundTicketForDelete2)
                && ticketRepository.isContains(foundTicketForNoDelete));
    }

    @Test
    public void testGetMoviesByCurrentDateOfDay() {
        Movie movie1 = createdMovie.get(0);
        Movie movie2 = createdMovie.get(1);
        Movie movie3 = createdMovie.get(2);
        Movie movie4 = createdMovie.get(3);
        List<Movie> result = List.of(movie1, movie2, movie3, movie4);
        List<Movie> moviesByCurrentDay = ticketServiceImpl.getMoviesByCurrentDateOfDay();
        Assert.assertEquals(moviesByCurrentDay, result);
    }

    @Test
    public void testGetUsersByDateOfDay() {
        User user1 = createdUser.get(2);
        User user2 = createdUser.get(3);
        User user3 = createdUser.get(0);
        List<User> result = List.of(user1, user2, user3);
        List<User> usersByDay = ticketServiceImpl.getUsersByDateOfDay(LocalDateTime.now());
        Assert.assertEquals(usersByDay, result);
    }

    @Test
    public void testGetSumByDateOfMonth() {
        float result = 110.1f;
        Assert.assertEquals(result, ticketServiceImpl.getSumByDateOfMonth(LocalDateTime.now()));
    }

    @Test
    public void testGetMoviesIfCountOfTicketsByMovieLessThenNumber() {
        Movie movie1 = createdMovie.get(1);
        Movie movie3 = createdMovie.get(3);
        List<Movie> result = List.of(movie1, movie3);
        Assert.assertEquals(result, ticketServiceImpl.getMoviesIfCountOfTicketsByMovieLessThenNumber(3));
    }

    @Test
    public void testGetSortedMoviesByCountOfTickets() {
        Movie movie1 = createdMovie.get(0);
        Movie movie2 = createdMovie.get(2);
        Movie movie3 = createdMovie.get(1);
        Movie movie4 = createdMovie.get(3);
        List<Movie> result = List.of(movie1, movie2, movie3, movie4);
        Assert.assertEquals(result, ticketServiceImpl.getSortedMoviesByCountOfTickets());
    }

//    @Test(dataProvider = "data-provider-correct-tickets-varargs")
//    public void testSaveTickets(Ticket... tickets) throws IOException, SQLException, ClassNotFoundException {
//        Assert.assertEquals(ticketRepository.getAll().size(), 0);
//        Arrays.stream(tickets)
//                .forEach(ticket -> {
//                    try {
//                        ticketRepository.create(ticket);
//                    } catch (SQLException throwables) {
//                        throwables.printStackTrace();
//                    }
//                });
//        Assert.assertEquals(ticketRepository.getAll().size(), tickets.length);
//        ticketServiceImpl.save();
//        ticketRepository.deleteAll();
//        Assert.assertEquals(ticketRepository.getAll().size(), 0);
//        ticketRepository = new JsonTicketRepositoryImpl(tempFile.getAbsolutePath());
//        ticketServiceImpl = new TicketServiceImpl(ticketRepository);
//        Assert.assertEquals(ticketRepository.getAll().size(), tickets.length);
//    }


}
