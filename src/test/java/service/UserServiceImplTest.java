package service;

import cinema.entities.User;
import cinema.entities.UserBuilder;

import cinema.repositories.db_repositories_impl.DbUserRepositoryImpl;
import cinema.repositories.interfaces.UserRepository;
import cinema.repositories.json_repositories_impl.JsonUserRepositoryImpl;

import cinema.services.services_impl.UserServiceImpl;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class UserServiceImplTest {
    private UserServiceImpl userServiceImpl;
    private UserRepository<User, UUID> userRepository;
    private List<User> createdUsers;

    private Connection connection;
    private File tempFile;

    @BeforeTest
    public void beforeTest() throws IOException, SQLException, ClassNotFoundException {
        Properties props = new Properties();
        String dbSettingsPropertyFile = "src\\test\\resources\\jdbc.properties";
        try(FileReader fReader = new FileReader(dbSettingsPropertyFile)) {
            props.load(fReader);
            String dbConnUrl = props.getProperty("db.conn.url");
            String dbUserName = props.getProperty("db.username");
            String dbPassword = props.getProperty("db.password");
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);
        }
        connection.setAutoCommit(false);

        tempFile = File.createTempFile("TempUsers", ".json");
        tempFile.deleteOnExit();

        createdUsers = new ArrayList<>();
        User user1 = new UserBuilder()
                .setName("person1")
                .setPhone("+38 (095) 12-34-567")
                .build();
        User user2 = new UserBuilder()
                .setName("person2")
                .setPhone("+38 (095) 12-34-568")
                .build();
        User user3 = new UserBuilder()
                .setName("person3")
                .setPhone("+38 (095) 12-34-569")
                .build();
        User user4 = new UserBuilder()
                .setName("person4")
                .setPhone("+38 (095) 12-34-570")
                .build();
        User user5 = new UserBuilder()
                .setName("person5")
                .setPhone("+38 (095) 12-34-571")
                .build();
        createdUsers.add(user1);
        createdUsers.add(user2);
        createdUsers.add(user3);
        createdUsers.add(user4);
        createdUsers.add(user5);
    }

//    @BeforeMethod
//    public void beforeMethodFile() throws IOException, SQLException {
//        userRepository = new JsonUserRepositoryImpl(tempFile.getAbsolutePath());
//        userRepository.deleteAll();
//        Arrays.stream(createdUsers.toArray())
//                .forEach(user -> {
//                    try {
//                        userRepository.create((User) user);
//                    } catch (SQLException throwables) {
//                        throwables.printStackTrace();
//                    }
//                });
//        userServiceImpl = new UserServiceImpl(userRepository);
//    }

    @BeforeMethod
    public void beforeMethodDb() throws IOException, SQLException, ClassNotFoundException {
        userRepository = new DbUserRepositoryImpl(connection);
        userRepository.deleteAll();
        Arrays.stream(createdUsers.toArray())
                .forEach(user -> {
                    try {
                        userRepository.create((User) user);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
        userServiceImpl = new UserServiceImpl(userRepository);
    }

    @Test
    public void testCorrectCreateNewUsers() throws SQLException {
        User user = new UserBuilder()
                .setName("user")
                .setPhone("+38 (095) 12-34-000")
                .build();
        userServiceImpl.create(user);
        Assert.assertTrue(userRepository.isContains(user));
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*User with this phone already exists.*")
    public void testIncorrectCreateNewUsersByPhone() {
        User user = new UserBuilder()
                .setName("user")
                .setPhone("+38 (095) 12-34-568")
                .build();
        userServiceImpl.create(user);
    }
    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*User with these settings already exists.*")
    public void testIncorrectCreateNewUsersByAllFields() {
        User user = new UserBuilder()
                .setName("person2")
                .setPhone("+38 (095) 12-34-568")
                .build();
        userServiceImpl.create(user);
    }
    @Test
    public void testGetAllUsers() {
        List<User> usersList = userServiceImpl.getAll();
        Assert.assertEquals(usersList, createdUsers);
    }

    @Test
    public void testCorrectGetUserById() {
        User user = createdUsers.get(0);
        User foundUser = userServiceImpl.getById(user.getId());
        Assert.assertEquals(foundUser, user);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*User with this id doesn't exist.*")
    public void testIncorrectGetUserById() {
        userServiceImpl.getById(UUID.randomUUID());
    }

    @Test
    public void testIsExistsUserById() {
        User user = createdUsers.get(0);
        Assert.assertTrue(userServiceImpl.isExistsById(user.getId()));
    }

    @Test
    public void testIsNotExistsUserById() {
        Assert.assertFalse(userServiceImpl.isExistsById(UUID.randomUUID()));
    }

    @Test
    public void testIsContainsUser() {
        User user = createdUsers.get(0);
        Assert.assertTrue(userServiceImpl.isContains(user));
    }

    @Test
    public void testIsNotContainsUser() {
        User user = new UserBuilder()
                .setName("user1")
                .setPhone("+38 (095) 12-34-122")
                .build();
        Assert.assertFalse(userServiceImpl.isContains(user));
    }

    @Test
    public void testCorrectUpdateUser() throws SQLException {
        User userFromService = createdUsers.get(0);
        User userNew = new UserBuilder()
                .setName("user")
                .setPhone("+38 (095) 12-34-111")
                .build();
        userNew.setId(userFromService.getId());
        userNew.setName("new name");
        userNew.setPhone(userFromService.getPhone());
        userServiceImpl.update(userNew);
        Assert.assertEquals(userRepository.getById(userFromService.getId()), userNew);
    }

    @Test
    public void testCorrectUpdateUserIfFieldsNoChange() throws SQLException {
        User userFromService = createdUsers.get(0);
        Assert.assertEquals(userRepository.getById(userFromService.getId()), userFromService);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*User with this phone already exists.*")
    public void testIncorrectUpdatePhoneUser() {
        User userFromService1 = new User(createdUsers.get(0));
        User userFromService2 = new User(createdUsers.get(1));
        userFromService1.setPhone(userFromService2.getPhone());
        userServiceImpl.update(userFromService1);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*User with these settings already exists.*")
    public void testIncorrectUpdateAllFieldsUser() {
        User userFromService1 = new User(createdUsers.get(0));
        User userFromService2 = new User(createdUsers.get(1));
        userFromService1.setName(userFromService2.getName());
        userFromService1.setPhone(userFromService2.getPhone());
        userServiceImpl.update(userFromService1);
    }

    @Test
    public void testCorrectDeleteUserById() throws SQLException {
        User userFromService = new User(createdUsers.get(0));
        userServiceImpl.deleteById(userFromService.getId());
        Assert.assertFalse(userRepository.isContains(userFromService));
    }

    @Test
    public void testCorrectDeleteAllUsers() throws SQLException {
        userServiceImpl.deleteAll();
        Assert.assertEquals(userRepository.getAll().size(), 0);
    }

//    @Test
//    public void testSaveUsersFile() throws IOException, SQLException {
//        userServiceImpl.save();
//        userRepository = new JsonUserRepositoryImpl(tempFile.getAbsolutePath());
//        Assert.assertEquals(userRepository.getAll().size(), createdUsers.size());
//    }

    @Test
    public void testSaveUsersDB() throws SQLException {
        userServiceImpl.save();
        userRepository = new DbUserRepositoryImpl(connection);
        Assert.assertEquals(userRepository.getAll().size(), createdUsers.size());
    }
}
