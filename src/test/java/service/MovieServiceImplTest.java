package service;

import cinema.entities.*;
import cinema.repositories.db_repositories_impl.DbMovieRepositoryImpl;

import cinema.repositories.db_repositories_impl.DbUserRepositoryImpl;
import cinema.repositories.interfaces.MovieRepository;
import cinema.repositories.json_repositories_impl.JsonMovieRepositoryImpl;
import cinema.services.services_impl.MovieServiceImpl;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.util.*;

public class MovieServiceImplTest {
    private MovieServiceImpl movieServiceImpl;
    private MovieRepository<Movie, UUID> movieRepository;
    private List<Movie> createdMovies;

    private Connection connection;
    private File tempFile;

    @BeforeTest
    public void beforeTest() throws IOException, SQLException, ClassNotFoundException {
        Properties props = new Properties();
        String dbSettingsPropertyFile = "src\\test\\resources\\jdbc.properties";
        try(FileReader fReader = new FileReader(dbSettingsPropertyFile)) {
            props.load(fReader);
            String dbConnUrl = props.getProperty("db.conn.url");
            String dbUserName = props.getProperty("db.username");
            String dbPassword = props.getProperty("db.password");
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);
        }
        connection.setAutoCommit(false);

        tempFile = File.createTempFile("TempMovie", ".json");
        tempFile.deleteOnExit();

        createdMovies = new ArrayList<>();
        Movie movie1 = new MovieBuilder()
                .setTitle("movie1")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        Movie movie2 = new MovieBuilder()
                .setTitle("movie2")
                .setDuration(Duration.ofMinutes(56))
                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
                .setRating(1.1f)
                .build();
        Movie movie3 = new MovieBuilder()
                .setTitle("movie3")
                .setDuration(Duration.ofMinutes(11))
                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
                .setRating(2f)
                .build();
        Movie movie4 = new MovieBuilder()
                .setTitle("movie4")
                .setDuration(Duration.ofMinutes(125))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(6.6f)
                .build();
        Movie movie5 = new MovieBuilder()
                .setTitle("movie5")
                .setDuration(Duration.ofMinutes(64))
                .setGenres(Set.of(Genre.FANTASY, Genre.DRAMA))
                .setRating(5.5f)
                .build();
        createdMovies.add(movie1);
        createdMovies.add(movie2);
        createdMovies.add(movie3);
        createdMovies.add(movie4);
        createdMovies.add(movie5);
    }

//    @BeforeMethod
//    public void beforeMethodFile() throws IOException, SQLException {
//        movieRepository = new JsonMovieRepositoryImpl(tempFile.getAbsolutePath());
//        movieRepository.deleteAll();
//        Arrays.stream(createdMovies.toArray())
//                .forEach(movie -> {
//                    try {
//                        movieRepository.create((Movie) movie);
//                    } catch (SQLException throwables) {
//                        throwables.printStackTrace();
//                    }
//                });
//        movieServiceImpl = new MovieServiceImpl(movieRepository);
//    }


    @BeforeMethod
    public void beforeMethodDb() throws IOException, SQLException, ClassNotFoundException {
        movieRepository = new DbMovieRepositoryImpl(connection);
        movieRepository.deleteAll();
        Arrays.stream(createdMovies.toArray())
                .forEach(movie -> {
                    try {
                        movieRepository.create((Movie) movie);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
        movieServiceImpl = new MovieServiceImpl(movieRepository);
    }

    @Test
    public void testCorrectCreateNewMovies() throws SQLException {
        Movie movie = new MovieBuilder()
                .setTitle("some movie")
                .setDuration(Duration.ofMinutes(64))
                .setGenres(Set.of(Genre.FANTASY, Genre.DRAMA))
                .setRating(5.5f)
                .build();
        movieServiceImpl.create(movie);
        Assert.assertTrue(movieRepository.isContains(movie));
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Movie with these settings already exists.*")
    public void testIncorrectCreateNewMovies() {
        Movie movie = new MovieBuilder()
                .setTitle("movie1")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        movieServiceImpl.create(movie);
    }

    @Test
    public void testGetAllMovies() {
        List<Movie> movies = movieServiceImpl.getAll();
        Assert.assertEquals(movies, createdMovies);
    }

    @Test
    public void testCorrectGetMovieById() throws SQLException {
        Movie movie = createdMovies.get(0);
        Movie foundMovie = movieServiceImpl.getById(movie.getId());
        Assert.assertEquals(foundMovie, movie);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Movie with this id doesn't exist.*")
    public void testIncorrectGetMovieById() {
        movieServiceImpl.getById(UUID.randomUUID());
    }

    @Test
    public void testIsExistsMovieById() throws SQLException {
        Movie movie = createdMovies.get(0);
        Assert.assertTrue(movieServiceImpl.isExistsById(movie.getId()));
    }

    @Test
    public void testIsNotExistsMovieById() throws SQLException {
        Assert.assertFalse(movieServiceImpl.isExistsById(UUID.randomUUID()));
    }

    @Test
    public void testIsContainsMovie() throws SQLException {
        Movie movie = createdMovies.get(0);
        Assert.assertTrue(movieServiceImpl.isContains(movie));
    }

    @Test
    public void testIsNotContainsUser() {
        Movie movie = new MovieBuilder()
                .setTitle("new movie")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        Assert.assertFalse(movieServiceImpl.isContains(movie));
    }

    @Test
    public void testCorrectUpdateMovie() throws SQLException {
        Movie movieFromService = createdMovies.get(0);
        Movie movieNew = new MovieBuilder()
                .setTitle("new movie")
                .setDuration(Duration.ofMinutes(34))
                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
                .setRating(1.1f)
                .build();
        movieNew.setId(movieFromService.getId());
        movieNew.setTitle("new name");
        movieNew.setDuration(movieFromService.getDuration());
        movieNew.setGenres(movieFromService.getGenres());
        movieNew.setRating(movieFromService.getRating());
        movieServiceImpl.update(movieNew);
        Assert.assertEquals(movieRepository.getById(movieFromService.getId()), movieNew);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}
            , expectedExceptionsMessageRegExp = ".*Movie with these settings already exists.*")
    public void testIncorrectUpdateAllFieldsMovie() {
        Movie movieFromService1 = new Movie(createdMovies.get(0));
        Movie movieFromService2 = new Movie(createdMovies.get(1));
        movieFromService2.setTitle(movieFromService1.getTitle());
        movieFromService2.setDuration(movieFromService1.getDuration());
        movieFromService2.setGenres(movieFromService1.getGenres());
        movieFromService2.setRating(movieFromService1.getRating());
        movieServiceImpl.update(movieFromService2);
    }

    @Test
    public void testCorrectDeleteMovieById() throws SQLException {
        Movie movie = createdMovies.get(0);
        movieServiceImpl.deleteById(movie.getId());
        Assert.assertFalse(movieRepository.isContains(movie));
    }

    @Test
    public void testCorrectDeleteAllMovies() throws SQLException {
        movieServiceImpl.deleteAll();
        Assert.assertEquals(movieRepository.getAll().size(), 0);
    }

    @Test
    public void testSaveMoviesDB() throws SQLException {
        movieServiceImpl.save();
        movieRepository = new DbMovieRepositoryImpl(connection);
        Assert.assertEquals(movieRepository.getAll().size(), createdMovies.size());
    }

//    @Test
//    public void testSaveMoviesFile() throws IOException, SQLException {
//        movieServiceImpl.save();
//        movieRepository = new JsonMovieRepositoryImpl(tempFile.getAbsolutePath());
//        Assert.assertEquals(movieRepository.getAll().size(), createdMovies.size());
//    }
}
