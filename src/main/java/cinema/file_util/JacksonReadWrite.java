package cinema.file_util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class JacksonReadWrite<T> {

    private String fileName;

    public JacksonReadWrite(String fileName) {
        this.fileName = fileName;
    }

    public void write(List<T> objects) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.writeValue(new File(fileName), objects.toArray());
    }
    public List<T> read(Class<T[]> classType) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        try {
            String content = new String(Files.readAllBytes(Paths.get(fileName)));
            T[] objectsList = objectMapper.readValue(content, classType);
            return new ArrayList<>(Arrays.asList(objectsList));
        } catch (NoSuchFileException e) {
            File yourFile = new File(fileName);
            yourFile.createNewFile();
            return new ArrayList<>();
        } catch (MismatchedInputException e) {
            return new ArrayList<>();
        }
    }

}
