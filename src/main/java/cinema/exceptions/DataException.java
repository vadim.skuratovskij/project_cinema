package cinema.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

public class DataException extends RuntimeException {

    private static final Logger LOGGER = LogManager.getLogger("DataException");

    public DataException(Exception exception) {
        super(exception);
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        LOGGER.error(exceptionAsString);
        LOGGER.info(exceptionAsString);
    }
}
