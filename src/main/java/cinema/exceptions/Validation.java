package cinema.exceptions;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class Validation<T> {

    public void isValid(T object) {
        ValidatorFactory validatorFactory = javax.validation.Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(object);
        if(!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            violations.forEach(violation -> sb.append(violation.getMessage()).append(", "));
            throw new InvalidFieldException("Object of " + object.getClass().getName() + " isn't valid: " + sb);
        }
    }

}
