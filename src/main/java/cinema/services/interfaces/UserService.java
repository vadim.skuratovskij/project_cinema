package cinema.services.interfaces;

public interface UserService<T, Id> extends CrudService<T, Id> {

    void checkNotExistsByPhone(String phone);

}
