package cinema.services.interfaces;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CrudService<T, Id> {

    List<T> getAll() throws SQLException, ClassNotFoundException;

    T getById(Id id) throws SQLException;

    boolean isExistsById(Id id);

    boolean isContains(T object);

    void deleteAll();

    void create(T object);

    void update(T object);

    void deleteById(Id id);

    void save() throws IOException;

}
