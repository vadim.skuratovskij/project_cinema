package cinema.services.interfaces;

import cinema.entities.Movie;
import cinema.entities.User;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public interface TicketService<T, Id> extends CrudService<T, Id> {

    void deleteByUser(User user) throws SQLException;

    void deleteByMovie(Movie movie) throws SQLException;

    List<User> getUsersByDateOfDay(LocalDateTime date) throws SQLException;

    List<Movie> getMoviesByCurrentDateOfDay() throws SQLException;

    float getSumByDateOfMonth(LocalDateTime date) throws SQLException;

    List<Movie> getSortedMoviesByCountOfTickets() throws SQLException;

    List<Movie> getMoviesIfCountOfTicketsByMovieLessThenNumber(long number) throws SQLException;

}
