package cinema.services.services_impl;

import cinema.entities.Movie;
import cinema.exceptions.DataException;
import cinema.repositories.interfaces.MovieRepository;
import cinema.repositories.json_repositories_impl.JsonMovieRepositoryImpl;
import cinema.services.interfaces.MovieService;
import lombok.SneakyThrows;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class MovieServiceImpl implements MovieService<Movie, UUID> {
    private MovieRepository<Movie, UUID> movieRepository;

    public MovieServiceImpl() {
        try {
            movieRepository = new JsonMovieRepositoryImpl();
        }
        catch (IOException ex) {
            throw new DataException(ex);
        }
    }

    public MovieServiceImpl(MovieRepository<Movie, UUID> movieRepositoryImpl) {
        this.movieRepository = movieRepositoryImpl;
    }

    @Override
    public List<Movie> getAll() {
        try {
            return movieRepository.getAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public Movie getById(UUID id) {
        try {
            movieRepository.checkExistsById(id);
            return movieRepository.getById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public boolean isExistsById(UUID id) {
        try {
            return movieRepository.isExistsById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public boolean isContains(Movie movie) {
        try {
            return movieRepository.isContains(movie);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteAll() {
        try {
            movieRepository.deleteAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void create(Movie movie) {
        try {
            movieRepository.checkNotContains(movie);
            movieRepository.create(movie);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void update(Movie movie) {
        try {
            Movie oldMovie = movieRepository.getById(movie.getId());
            if(movie.equals(oldMovie))
                return;
            movieRepository.checkNotContains(movie);
            movieRepository.update(movie);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteById(UUID id) {
        try {
            movieRepository.checkExistsById(id);
            movieRepository.deleteById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void save() {
        try {
            movieRepository.save();
        }
        catch (SQLException|IOException ex) {
            throw new DataException(ex);
        }
    }

}
