package cinema.services.services_impl;

import cinema.entities.Movie;
import cinema.entities.Ticket;
import cinema.entities.User;
import cinema.exceptions.DataException;
import cinema.repositories.interfaces.TicketRepository;
import cinema.repositories.json_repositories_impl.JsonTicketRepositoryImpl;
import cinema.services.interfaces.TicketService;
import lombok.SneakyThrows;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class TicketServiceImpl implements TicketService<Ticket, UUID> {
    private TicketRepository<Ticket, UUID> ticketRepository;

    public TicketServiceImpl() {
        try {
            ticketRepository = new JsonTicketRepositoryImpl();
        }
        catch (IOException ex) {
            throw new DataException(ex);
        }
    }

    public TicketServiceImpl(TicketRepository<Ticket, UUID> ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public List<Ticket> getAll() {
        try {
            return ticketRepository.getAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public Ticket getById(UUID id) {
        try {
            return ticketRepository.getById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public boolean isExistsById(UUID id) {
        try {
            return ticketRepository.isExistsById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public boolean isContains(Ticket ticket) {
        try {
            return ticketRepository.isContains(ticket);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteAll() {
        try {
            ticketRepository.deleteAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void create(Ticket ticket) {
        try {
            ticketRepository.checkNotContains(ticket);
            ticketRepository.create(ticket);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void update(Ticket newTicket) {
        try {
            Ticket oldTicket = ticketRepository.getById(newTicket.getId());
            if(oldTicket.equals(newTicket))
                return;
            ticketRepository.checkNotContains(newTicket);
            ticketRepository.update(newTicket);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteById(UUID id) {
        try {
            ticketRepository.checkExistsById(id);
            ticketRepository.deleteById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void save() {
        try {
            ticketRepository.save();
        }
        catch (SQLException|IOException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteByUser(User user) {
        try {
            ticketRepository.deleteByUser(user);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteByMovie(Movie movie) {
        try {
            ticketRepository.deleteByMovie(movie);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public List<Movie> getMoviesByCurrentDateOfDay() {
        try {
            return ticketRepository.getMoviesByCurrentDateOfDay();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public List<User> getUsersByDateOfDay(LocalDateTime date) {
        try {
            return ticketRepository.getUsersByDateOfDay(date);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public float getSumByDateOfMonth(LocalDateTime date) {
        try {
            return ticketRepository.getSumByDateOfMonth(date);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public List<Movie> getSortedMoviesByCountOfTickets() {
        try {
            return ticketRepository.getSortedMoviesByCountOfTickets();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public List<Movie> getMoviesIfCountOfTicketsByMovieLessThenNumber(long number) {
        try {
            return ticketRepository.getMoviesIfCountOfTicketsByMovieLessThenNumber(number);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

}
