package cinema.services.services_impl;

import cinema.entities.User;

import cinema.exceptions.DataException;
import cinema.repositories.interfaces.UserRepository;
import cinema.repositories.json_repositories_impl.JsonUserRepositoryImpl;
import cinema.services.interfaces.UserService;
import lombok.SneakyThrows;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class UserServiceImpl implements UserService<User, UUID> {
    private final UserRepository<User, UUID> userRepository;

    public UserServiceImpl() {
        try {
            userRepository = new JsonUserRepositoryImpl();
        }
        catch (IOException ex) {
            throw new DataException(ex);
        }
    }

    public UserServiceImpl(UserRepository<User, UUID> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        try {
            return userRepository.getAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }


    @Override
    public User getById(UUID id) {
        try {
            userRepository.checkExistsById(id);
            return userRepository.getById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }


    @Override
    public boolean isExistsById(UUID id) {
        try {
            return userRepository.isExistsById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }


    @Override
    public boolean isContains(User user) {
        try {
            return userRepository.isContains(user);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteAll() {
        try {
            userRepository.deleteAll();
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void create(User user) {
        try {
            userRepository.checkNotContains(user);
            checkNotExistsByPhone(user.getPhone());
            userRepository.create(user);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void update(User user) {
        try {
        User oldUser = userRepository.getById(user.getId());
        if(user.equals(oldUser))
            return;
        userRepository.checkNotContains(user);
        if(!oldUser.getPhone().equals(user.getPhone()))
            checkNotExistsByPhone(user.getPhone());
        userRepository.update(user);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void deleteById(UUID id) {
        try {
            userRepository.checkExistsById(id);
            userRepository.deleteById(id);
        }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void save() {
        try {
            userRepository.save();
        }
        catch (SQLException|IOException ex) {
            throw new DataException(ex);
        }
    }

    @Override
    public void checkNotExistsByPhone(String phone) {
        try {
            if(userRepository.isExistsByPhone(phone))
                throw new IllegalArgumentException("User with this phone already exists");
            }
        catch (SQLException ex) {
            throw new DataException(ex);
        }
    }
}
