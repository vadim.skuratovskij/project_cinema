package cinema.entities;

public enum Genre {
    COMEDY("something about comedy"),
    DRAMA("something about drama"),
    FANTASY("something about fantasy"),
    HORROR("something about horror");

    private final String description;

    private Genre(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
