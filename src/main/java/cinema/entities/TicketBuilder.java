package cinema.entities;

import cinema.exceptions.Validation;

import java.time.LocalDateTime;
import java.util.UUID;

public class TicketBuilder {
    private Ticket ticket = new Ticket();

    public TicketBuilder setUser(User user) {
        ticket.setUser(user);
        return this;
    }

    public TicketBuilder setMovie(Movie movie) {
        ticket.setMovie(movie);
        return this;
    }

    public TicketBuilder setDate(LocalDateTime date) {
        ticket.setDate(date);
        return this;
    }

    public TicketBuilder setPrice(float price) {
        ticket.setPrice(price);
        return this;
    }

    public Ticket build() {
        if(ticket.getDate() == null)
            ticket.setDate(LocalDateTime.now());
        new Validation<Ticket>().isValid(ticket);
        ticket.setId(UUID.randomUUID());
        return ticket;
    }
}
