package cinema.entities;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
public class Ticket {
    private UUID id;

    @NotNull(message = "movie cannot be null")
    private Movie movie;

    @NotNull(message = "user cannot be null")
    private User user;

    @NotNull(message = "date cannot be null")
    @PastOrPresent(message = "the date must be past or present")
    private LocalDateTime date;

    @NotNull(message = "price cannot be null")
    @PositiveOrZero(message = "price must be positive or zero")
    private float price;

    public Ticket() {
    }

    public Ticket(UUID id, Movie movie, User user, LocalDateTime date, float price) {
        this.id = id;
        this.movie = movie;
        this.user = user;
        this.date = date;
        this.price = price;
    }

    public Ticket(Ticket ticket) {
        this(ticket.getId(), ticket.getMovie(), ticket.getUser(), ticket.getDate(), ticket.getPrice());
    }

    @Override
    public String toString() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return "{id = " + id +
                ", movie.id = " + movie.getId() +
                "(" + movie.getTitle() + ")" +
                ", user.id = " + user.getId() +
                "(" + user.getName() + ")" +
                ", date = " + date.format(dateFormat) +
                ", price = " + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(getMovie(), ticket.getMovie()) && Objects.equals(getUser(), ticket.getUser()) && Objects.equals(getDate().toLocalDate(), ticket.getDate().toLocalDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMovie(), getUser(), getDate());
    }
}
