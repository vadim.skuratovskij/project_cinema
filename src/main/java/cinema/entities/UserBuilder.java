package cinema.entities;

import cinema.exceptions.Validation;

import java.util.UUID;

public class UserBuilder {
    private User user = new User();

    public UserBuilder setName(String name) {
        user.setName(name);
        return this;
    }
    public UserBuilder setPhone(String phone) {
        user.setPhone(phone);
        return this;
    }

    public User build() {
        new Validation<User>().isValid(user);
        user.setPhone(toPhoneFormat(user.getPhone()));
        user.setId(UUID.randomUUID());
        return user;
    }

    private String toPhoneFormat(String phone) {
        if(phone != null) {
            String phoneOnlyDigits = phone.replaceAll("\\D", "");
            if (phoneOnlyDigits.length() == 12) {
                phoneOnlyDigits = phoneOnlyDigits.substring(2);
            }
            return phoneOnlyDigits;
        }
        return null;
    }
}
