package cinema.entities;

import cinema.exceptions.Validation;

import java.time.Duration;
import java.util.Set;
import java.util.UUID;

public class MovieBuilder {
    private Movie movie = new Movie();

    public MovieBuilder setTitle(String title) {
        movie.setTitle(title);
        return this;
    }
    public MovieBuilder setDuration(Duration duration) {
        movie.setDuration(duration);
        return this;
    }
    public MovieBuilder setGenres(Set<Genre> genres) {
        movie.setGenres(genres);
        return this;
    }
    public MovieBuilder setRating(float rating) {
        movie.setRating(rating);
        return this;
    }

    public Movie build() {
        new Validation<Movie>().isValid(movie);
        movie.setId(UUID.randomUUID());
        return movie;
    }
}
