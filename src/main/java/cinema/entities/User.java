package cinema.entities;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;
import java.util.UUID;

@Setter
@Getter
//@Table("user")
public class User {

    private UUID id;

    @NotBlank(message = "title cannot be blank")
    private String name;
    @Pattern(regexp = "^([+]?38[ ]?)?((\\([\\d]{3}\\))|([\\d]{3}))[ ]?([\\d]{2}[ .-]?){2}[\\d]{3}$", message = "phone has wrong format")
    private String phone;

    public User() {
    }

    public User(UUID id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public User(User user) {
        this(user.getId(), user.getName(), user.getPhone());
    }

    @Override
    public String toString() {
        return "{id = " + id +
                ", name = '" + name + '\'' +
                ", phone = '" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getName(), user.getName()) && Objects.equals(getPhone(), user.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPhone());
    }
}
