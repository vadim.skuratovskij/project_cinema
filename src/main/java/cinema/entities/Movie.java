package cinema.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
@Getter
public class Movie {
    private UUID id;

    @NotBlank(message = "title cannot be blank")
    private String title;

    @NotNull(message = "duration cannot be null")
    private Duration duration;

    private Set<Genre> genres;

    @Min(value = 0, message = "rating cannot be less than 0")
    @Max(value = 10, message = "rating cannot be bigger than 10")
    private float rating;

    public Movie() {
    }

    public Movie(UUID id, String title, Duration duration, Set<Genre> genres, float rating) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.genres = genres;
        this.rating = rating;
    }

    public Movie(Movie movie) {
        this(movie.getId(), movie.getTitle(), movie.getDuration(), movie.getGenres(), movie.getRating());
    }


    public void setDuration(Duration duration) {
        this.duration = duration;
    }
    @JsonIgnore
    public void setDuration(long duration) {
        this.duration = Duration.ofSeconds(duration);
    }

    @JsonSetter("genres")
    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }
    @JsonIgnore
    public void setGenres(String[] genres) {
        this.genres = Arrays.stream(genres)
                .map(Genre::valueOf)
                .collect(Collectors.toSet());
    }

    @JsonGetter("genres")
    public Set<Genre> getGenres() {
        return genres;
    }
    @JsonIgnore
    public String[] getStringsGenres() {
        String[] stringsGenres = Arrays.stream(genres.toArray())
                .map(Object::toString)
                .toArray(String[]::new);
        return stringsGenres;
    }


    @Override
    public String toString() {
        return "{id = " + id +
                ", title = '" + title + '\'' +
                ", duration = " + (duration.getSeconds() / 60) +
                ", genres = " + genres +
                ", rating = " + rating +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return Objects.equals(getTitle(), movie.getTitle()) && Objects.equals(getDuration(), movie.getDuration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getDuration());
    }
}
