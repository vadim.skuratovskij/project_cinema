package cinema.repositories.interfaces;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CrudRepository<T, Id> {

   List<T> getAll() throws SQLException;

    T getById(Id id) throws IllegalArgumentException, SQLException;

    boolean isExistsById(Id id) throws SQLException;

    void checkExistsById(Id id) throws IllegalArgumentException, SQLException;

    boolean isContains(T object) throws SQLException;

    void checkNotContains(T object) throws IllegalArgumentException, SQLException;

    void deleteAll() throws SQLException;

    void create(T object) throws SQLException;

    void update(T object) throws SQLException;

    void deleteById(Id id) throws SQLException;

    void save() throws IOException, SQLException;

}
