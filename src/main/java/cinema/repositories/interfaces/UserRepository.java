package cinema.repositories.interfaces;

import java.sql.SQLException;

public interface UserRepository<T, Id> extends CrudRepository<T, Id> {
   boolean isExistsByPhone(String phone) throws SQLException;
}
