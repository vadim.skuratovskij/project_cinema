package cinema.repositories.db_repositories_impl;

import cinema.db_util.DbHandler;
import cinema.entities.User;
import cinema.repositories.interfaces.UserRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DbUserRepositoryImpl implements UserRepository<User, UUID> {

    private final Connection connection;

    public DbUserRepositoryImpl() throws SQLException {
        connection = DbHandler.getInstanceConnection();
    }

    public DbUserRepositoryImpl(Connection connection) throws SQLException {
        this.connection = connection;
    }

    @Override
    public List<User> getAll() throws SQLException {
        try(Statement statement = connection.createStatement()) {
            List<User> users = new ArrayList<>();
            ResultSet rs = statement.executeQuery(QUERY_GET_ALL_USERS);
            while(rs.next()) {
                User user = new User();
                user.setId(UUID.fromString(rs.getString("id")));
                user.setName(rs.getString("name"));
                user.setPhone(rs.getString("phone"));
                users.add(user);
            }
            return users;
        }
    }

    @Override
    public User getById(UUID id) throws IllegalArgumentException, SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_USER_BY_ID)) {
            User user = new User();
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user.setId(UUID.fromString(rs.getString("id")));
                user.setName(rs.getString("name"));
                user.setPhone(rs.getString("phone"));
            } else {
                throw new IllegalArgumentException("User with this id doesn't exist");
            }
            return user;
        }
    }

    @Override
    public boolean isExistsById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_USER_BY_ID)) {
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException, SQLException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("User with this id doesn't exist");
    }

    @Override
    public boolean isContains(User user) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_USERS_BY_FIELDS)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPhone());
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkNotContains(User user) throws IllegalArgumentException, SQLException {
        if(isContains(user))
            throw new IllegalArgumentException("User with these settings already exists");
    }

    @Override
    public void deleteAll() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(QUERY_DELETE_ALL_USERS);
        }
    }

    @Override
    public void create(User user) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT_INTO_USER)) {
            preparedStatement.setObject(1, user.getId()); // , java.sql.Types.OTHER
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getPhone());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void update(User user) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE_USER)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPhone());
            preparedStatement.setObject(3, user.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void deleteById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_DELETE_USERS_BY_ID)) {
            preparedStatement.setObject(1, id);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void save() throws SQLException {
        connection.commit();
    }

    @Override
    public boolean isExistsByPhone(String phone) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_USERS_BY_PHONE)) {
            preparedStatement.setString(1, phone);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    private final String QUERY_GET_ALL_USERS =
            "SELECT * FROM public.user";

    private final String QUERY_GET_USER_BY_ID =
            "SELECT * FROM public.user" +
            " Where id = ?";

    private final String QUERY_GET_USERS_BY_FIELDS =
            "SELECT * FROM public.user" +
            " WHERE name = ?" +
            " AND phone = ?";

    private final String QUERY_DELETE_ALL_USERS =
            "DELETE FROM public.user";

    private final String QUERY_DELETE_USERS_BY_ID =
            "DELETE FROM public.user" +
            " WHERE id = ?";

    private final String QUERY_INSERT_INTO_USER =
            "INSERT INTO public.user (id, name, phone)" +
            " VALUES(?,?,?)";

    private final String QUERY_UPDATE_USER =
            "UPDATE public.user" +
            " SET name = ?, phone = ?" +
            " WHERE id = ?";

    private final String QUERY_GET_USERS_BY_PHONE =
            "SELECT * FROM public.user" +
            " WHERE phone = ?";
}
