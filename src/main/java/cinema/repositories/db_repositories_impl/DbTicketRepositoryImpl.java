package cinema.repositories.db_repositories_impl;

import cinema.db_util.DbHandler;
import cinema.entities.Movie;
import cinema.entities.Ticket;
import cinema.entities.User;
import cinema.repositories.interfaces.TicketRepository;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DbTicketRepositoryImpl implements TicketRepository<Ticket, UUID> {

    private final Connection connection;

    public DbTicketRepositoryImpl() throws SQLException {
        connection = DbHandler.getInstanceConnection();
    }

    public DbTicketRepositoryImpl(Connection connection) throws SQLException {
        this.connection = connection;
    }

    @Override
    public List<Ticket> getAll() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            List<Ticket> tickets = new ArrayList<>();
            ResultSet rs = statement.executeQuery(queryGetAllTickets);
            while (rs.next()) {
                Ticket ticket = getTicketFromResultSet(rs);
                tickets.add(ticket);
            }
            return tickets;
        }
    }

    @Override
    public Ticket getById(UUID id) throws IllegalArgumentException, SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetTicketById)) {
            Ticket ticket;
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                ticket = getTicketFromResultSet(rs);
            } else {
                throw new IllegalArgumentException("Ticket with this id doesn't exist");
            }
            return ticket;
        }
    }

    @Override
    public boolean isExistsById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetTicketById)) {
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException, SQLException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("Ticket with this id doesn't exist");
    }

    @Override
    public boolean isContains(Ticket ticket) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetTicketByFields)) {
            preparedStatement.setObject(1, ticket.getUser().getId());
            preparedStatement.setObject(2, ticket.getMovie().getId());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(ticket.getDate()));
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkNotContains(Ticket ticket) throws IllegalArgumentException, SQLException {
        if(isContains(ticket))
            throw new IllegalArgumentException("Ticket with these settings already exists");
    }

    @Override
    public void deleteAll() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(queryClearTickets);
        }
    }

    @Override
    public void create(Ticket ticket) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryInsertIntoTicket)) {
            preparedStatement.setObject(1, ticket.getId()); // , java.sql.Types.OTHER
            preparedStatement.setFloat(2, ticket.getPrice());
            preparedStatement.setTimestamp(3, Timestamp.valueOf(ticket.getDate()));
            preparedStatement.setObject(4, ticket.getUser().getId());
            preparedStatement.setObject(5, ticket.getMovie().getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void update(Ticket ticket) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryUpdateTicketById)) {
            preparedStatement.setFloat(1, ticket.getPrice());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(ticket.getDate()));
            preparedStatement.setObject(3, ticket.getUser().getId());
            preparedStatement.setObject(4, ticket.getMovie().getId());
            preparedStatement.setObject(5, ticket.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void deleteById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryDeleteTicketById)) {
            preparedStatement.setObject(1, id);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void save() throws IOException, SQLException {
        connection.commit();
    }

    @Override
    public void deleteByUser(User user) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryDeleteTicketByUser)) {
            preparedStatement.setObject(1, user.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void deleteByMovie(Movie movie) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryDeleteTicketByMovie)) {
            preparedStatement.setObject(1, movie.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<User> getUsersByDateOfDay(LocalDateTime date) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetUsersByDateOfDay)) {
            List<User> users = new ArrayList<>();
            preparedStatement.setDate(1, Date.valueOf(date.toLocalDate()));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                User user = getUserFromResultSet(rs);
                users.add(user);
            }
            return users;
        }
    }

    @Override
    public List<Movie> getMoviesByCurrentDateOfDay() throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetMoviesByCurrentDateOfDay)) {
            List<Movie> movies = new ArrayList<>();
            preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }

    @Override
    public float getSumByDateOfMonth(LocalDateTime date) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetSumByDateOfMonth)) {
            preparedStatement.setInt(1, date.getYear());
            preparedStatement.setInt(2, date.getMonth().getValue());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getFloat("sum");
            }
            return 0;
        }
    }

    @Override
    public List<Movie> getSortedMoviesByCountOfTickets() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            List<Movie> movies = new ArrayList<>();
            ResultSet rs = statement.executeQuery(queryGetSortedMoviesByCountOfTickets);
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }

    @Override
    public List<Movie> getMoviesIfCountOfTicketsByMovieLessThenNumber(long number) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryGetMoviesIfCountOfTicketsByMovieLessThenNumber)) {
            List<Movie> movies = new ArrayList<>();
            preparedStatement.setLong(1, number);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }

    private User getUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(UUID.fromString(rs.getString("user_id")));
        user.setName(rs.getString("user_name"));
        user.setPhone(rs.getString("user_phone"));
        return user;
    }

    private Movie getMovieFromResultSet(ResultSet rs) throws SQLException {
        Movie movie = new Movie();
        movie.setId(UUID.fromString(rs.getString("movie_id")));
        movie.setTitle(rs.getString("movie_title"));
        movie.setDuration(rs.getLong("movie_duration"));
        movie.setGenres((String[]) rs.getArray("movie_genres").getArray());
        movie.setRating(rs.getFloat("movie_rating"));
        return movie;
    }

    private Ticket getTicketFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        Movie movie = new Movie();
        Ticket ticket = new Ticket();
        user.setId(UUID.fromString(rs.getString("user_id")));
        user.setName(rs.getString("user_name"));
        user.setPhone(rs.getString("user_phone"));
        movie.setId(UUID.fromString(rs.getString("movie_id")));
        movie.setTitle(rs.getString("movie_title"));
        movie.setDuration(rs.getLong("movie_duration"));
        movie.setGenres((String[]) rs.getArray("movie_genres").getArray());
        movie.setRating(rs.getFloat("movie_rating"));
        ticket.setId(UUID.fromString(rs.getString("ticket_id")));
        ticket.setDate(rs.getTimestamp("ticket_date").toLocalDateTime());
        ticket.setPrice(rs.getFloat("ticket_price"));
        ticket.setUser(user);
        ticket.setMovie(movie);
        return ticket;
    }

    private final String queryGetAllTickets =
            "SELECT t.id AS ticket_id, t.date AS ticket_date , t.price AS ticket_price" +
            ", u.id AS user_id, u.name AS user_name, u.phone AS user_phone" +
            ", m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating" +
            " FROM ticket AS t" +
            " INNER JOIN public.user AS u ON u.id = t.user_id" +
            " INNER JOIN movie AS m ON t.movie_id = m.id";

    private final String queryGetTicketById =
            queryGetAllTickets +
            " WHERE t.id = ?";

    private final String queryGetTicketByFields =
            queryGetAllTickets +
            " WHERE t.user_id = ?" +
            " AND t.movie_id = ? " +
            " AND t.date = ?";

    private final String queryClearTickets =
            "TRUNCATE TABLE ticket";

    private final String queryDeleteTicketById =
            "DELETE FROM ticket" +
            " WHERE id = ?";

    private final String queryDeleteTicketByUser =
            "DELETE FROM ticket" +
            " WHERE user_id = ?";

    private final String queryDeleteTicketByMovie =
            "DELETE FROM ticket" +
            " WHERE movie_id = ?";

    private final String queryInsertIntoTicket =
            "INSERT INTO ticket (id, price, date, user_id, movie_id)" +
            " VALUES(?,?,?,?,?)";

    private final String queryUpdateTicketById =
            "UPDATE ticket" +
            " SET price = ?, date = ?, user_id = ?, movie_id = ?" +
            " WHERE id = ?";

    private final String queryGetUsersByDateOfDay =
            "SELECT DISTINCT u.id AS user_id, u.name AS user_name, u.phone AS user_phone" +
            " FROM ticket AS t" +
            " INNER JOIN public.user AS u ON u.id = t.user_id " +
            " WHERE CAST(t.Date AS date) = ?" +
            " ORDER BY u.name";

    private final String queryGetMoviesByCurrentDateOfDay =
            "SELECT DISTINCT m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating" +
            " FROM ticket AS t" +
            " INNER JOIN movie AS m ON m.id = t.movie_id " +
            " WHERE CAST(t.Date AS date) = ?" +
            " ORDER BY movie_title";

    private final String queryGetSumByDateOfMonth =
            "SELECT DISTINCT SUM(t.price) as sum" +
            "  FROM ticket t" +
            "  WHERE EXTRACT(YEAR FROM t.DATE) = ? AND EXTRACT(MONTH FROM t.DATE) = ?";

    private final String queryGetSortedMoviesByCountOfTickets =
            "SELECT m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating" +
            ", count(*) AS count" +
            " FROM ticket t" +
            " INNER JOIN movie m ON t.movie_id = m.id" +
            " GROUP BY m.id, m.title" +
            " ORDER BY count DESC, m.title";

    private final String queryGetMoviesIfCountOfTicketsByMovieLessThenNumber =
            "SELECT m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating" +
            ", COUNT(*) AS count" +
            " FROM ticket t" +
            " INNER JOIN movie m ON t.movie_id = m.id" +
            " GROUP BY m.id, m.title" +
            " HAVING COUNT(*) < ?" +
            " ORDER BY m.title";
}
