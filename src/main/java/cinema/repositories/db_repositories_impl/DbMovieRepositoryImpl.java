package cinema.repositories.db_repositories_impl;

import cinema.db_util.DbHandler;
import cinema.entities.Movie;
import cinema.repositories.interfaces.MovieRepository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DbMovieRepositoryImpl implements MovieRepository<Movie, UUID> {

    private final Connection connection;

    public DbMovieRepositoryImpl() throws SQLException {
        connection = DbHandler.getInstanceConnection();
    }

    public DbMovieRepositoryImpl(Connection connection) throws SQLException {
        this.connection = connection;
    }

    @Override
    public List<Movie> getAll() throws SQLException {
        try(Statement statement = connection.createStatement()) {
            List<Movie> movies = new ArrayList<>();
            ResultSet rs = statement.executeQuery(QUERY_GET_ALL_MOVIES);
            while(rs.next()) {
                Movie movie = new Movie();
                movie.setId(UUID.fromString(rs.getString("id")));
                movie.setTitle(rs.getString("title"));
                movie.setDuration(rs.getLong("duration"));
                movie.setGenres((String[]) rs.getArray("genres").getArray());
                movie.setRating(rs.getFloat("rating"));

                movies.add(movie);
            }
            return movies;
        }
    }

    @Override
    public Movie getById(UUID id) throws IllegalArgumentException, SQLException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_MOVIE_BY_ID)) {
            Movie movie = new Movie();
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                movie.setId(UUID.fromString(rs.getString("id")));
                movie.setTitle(rs.getString("title"));
                movie.setDuration(rs.getLong("duration"));
                movie.setGenres((String[]) rs.getArray("genres").getArray());
                movie.setRating(rs.getFloat("rating"));
            }
            else {
                throw new IllegalArgumentException("Movie with this id doesn't exist");
            }
            return movie;
        }
    }

    @Override
    public boolean isExistsById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_MOVIE_BY_ID)) {
            preparedStatement.setObject(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException, SQLException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("Movie with this id doesn't exist");
    }

    @Override
    public boolean isContains(Movie movie) throws SQLException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(QUERY_GET_MOVIE_BY_FIELDS)) {
            preparedStatement.setString(1, movie.getTitle());
            preparedStatement.setLong(2, movie.getDuration().getSeconds());
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public void checkNotContains(Movie movie) throws IllegalArgumentException, SQLException {
        if(isContains(movie))
            throw new IllegalArgumentException("Movie with these settings already exists");
    }

    @Override
    public void deleteAll() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(QUERY_DELETE_ALL_MOVIES);
        }
    }

    @Override
    public void create(Movie movie) throws SQLException {
        try(PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERY_INTO_MOVIE)) {
            preparedStatement.setObject(1, movie.getId());
            preparedStatement.setString(2, movie.getTitle());
            preparedStatement.setLong(3, movie.getDuration().getSeconds());
            preparedStatement.setArray(4,
                    connection.createArrayOf("text", movie.getStringsGenres()));
            preparedStatement.setFloat(5, movie.getRating());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void update(Movie movie) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE_MOVIE_BY_ID)) {
            preparedStatement.setString(1, movie.getTitle());
            preparedStatement.setLong(2, movie.getDuration().getSeconds());
            preparedStatement.setArray(3,
                    connection.createArrayOf("text", movie.getStringsGenres()));
            preparedStatement.setFloat(4, movie.getRating());
            preparedStatement.setObject(5, movie.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void deleteById(UUID id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_DELETE_MOVIE_BY_ID)) {
            preparedStatement.setObject(1, id);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void save() throws IOException, SQLException {
        connection.commit();
    }

    private final String QUERY_GET_ALL_MOVIES =
            "SELECT * FROM public.movie";

    private final String QUERY_GET_MOVIE_BY_ID =
            "SELECT * FROM movie " +
            " WHERE id = ?";

    private final String QUERY_GET_MOVIE_BY_FIELDS =
            "SELECT * FROM movie" +
            " WHERE title = ?" +
            " AND duration = ?";

    private final String QUERY_DELETE_ALL_MOVIES =
            "DELETE FROM movie";

    private final String QUERY_DELETE_MOVIE_BY_ID =
            "DELETE FROM movie" +
            " WHERE id = ?";

    private final String QUERY_INSERY_INTO_MOVIE =
            "INSERT INTO movie (id, title, duration, genres, rating)" +
            " VALUES(?,?,?,?,?)";

    private final String QUERY_UPDATE_MOVIE_BY_ID =
            "UPDATE movie" +
            " SET title = ?, duration = ?, genres = ?, rating = ?" +
            " WHERE id = ?";
}
