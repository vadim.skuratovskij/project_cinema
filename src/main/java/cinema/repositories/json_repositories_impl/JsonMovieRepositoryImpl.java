package cinema.repositories.json_repositories_impl;

import cinema.entities.Movie;
import cinema.file_util.JacksonReadWrite;
import cinema.repositories.interfaces.MovieRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class JsonMovieRepositoryImpl implements MovieRepository<Movie, UUID> {

    private List<Movie> movies;
    private final JacksonReadWrite<Movie> jacksonReadWrite;

    public JsonMovieRepositoryImpl() throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>("Movies.json");
        movies = jacksonReadWrite.read(Movie[].class);
    }

    public JsonMovieRepositoryImpl(String fileName) throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>(fileName);
        movies = jacksonReadWrite.read(Movie[].class);
    }

    @Override
    public List<Movie> getAll() {
        return movies.stream()
                .map(Movie::new)
                .collect(Collectors.toList());
    }

    @Override
    public Movie getById(UUID id) {
        return movies.stream()
                .filter(m -> m.getId().equals(id))
                .findFirst()
                .map(Movie::new)
                .orElseThrow(() -> new IllegalArgumentException("Movie with this id doesn't exist"));
    }

    @Override
    public boolean isExistsById(UUID id) {
        return movies.stream()
                .anyMatch(m -> m.getId().equals(id));
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("Movie with this id doesn't exist");

    }

    @Override
    public boolean isContains(Movie movie) throws IllegalArgumentException {
        return movies.stream()
                .anyMatch(u -> u.equals(movie));
    }

    @Override
    public void checkNotContains(Movie movie) throws IllegalArgumentException {
        if(isContains(movie))
            throw new IllegalArgumentException("Movie with these settings already exists");
    }

    @Override
    public void deleteAll() {
        movies = new ArrayList<>();
    }

    @Override
    public void create(Movie movie){
        movies.add(movie);
    }

    @Override
    public void update(Movie newMovie){
        Movie oldMovie = getById(newMovie.getId());
        movies.set(movies.indexOf(oldMovie), newMovie);
    }

    @Override
    public void deleteById(UUID id) {
        Movie movie = getById(id);
        movies.remove(movie);
    }

    @Override
    public void save() throws IOException {
        jacksonReadWrite.write(movies);
    }
}
