package cinema.repositories.json_repositories_impl;

import cinema.entities.User;
import cinema.file_util.JacksonReadWrite;
import cinema.repositories.interfaces.UserRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class JsonUserRepositoryImpl implements UserRepository<User, UUID> {

    private List<User> users;
    private final JacksonReadWrite<User> jacksonReadWrite;

    public JsonUserRepositoryImpl() throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>("Users.json");
        users = jacksonReadWrite.read(User[].class);
    }

    public JsonUserRepositoryImpl(String fileName) throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>(fileName);
        users = jacksonReadWrite.read(User[].class);
    }

    @Override
    public List<User> getAll() {
        return users.stream()
                .map(User::new)
                .collect(Collectors.toList());
    }

    @Override
    public User getById(UUID id) throws IllegalArgumentException {
        // todo
        return users.stream()
                .filter(u -> u.getId().equals(id))
                .findFirst()
                .map(User::new)
                .orElseThrow(() -> new IllegalArgumentException("User with this id doesn't exist"));
    }

    @Override
    public boolean isExistsById(UUID id) {
        return users.stream()
                .anyMatch(m -> m.getId().equals(id));
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("User with this id doesn't exist");
    }

    @Override
    public boolean isContains(User user) throws IllegalArgumentException {
        return users.stream()
                .anyMatch(u -> u.equals(user));
    }

    @Override
    public void checkNotContains(User user) throws IllegalArgumentException {
        if(isContains(user))
            throw new IllegalArgumentException("User with these settings already exists");
    }

    @Override
    public void deleteAll() {
        users = new ArrayList<>();
    }

    @Override
    public void create(User user) {
        users.add(user);
    }

    @Override
    public void update(User newUser) {
        User oldUser = getById(newUser.getId());
        users.set(users.indexOf(oldUser), newUser);
    }

    @Override
    public void deleteById(UUID id) {
        User user = getById(id);
        users.remove(user);
    }

    @Override
    public void save() throws IOException {

        jacksonReadWrite.write(users);
    }
    
    @Override
    public boolean isExistsByPhone(String phone) {
        return users.stream()
                .anyMatch(m -> m.getPhone().equals(phone));
    }


}
