package cinema.repositories.json_repositories_impl;

import cinema.entities.Movie;
import cinema.entities.Ticket;
import cinema.entities.User;
import cinema.file_util.JacksonReadWrite;
import cinema.repositories.interfaces.TicketRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JsonTicketRepositoryImpl implements TicketRepository<Ticket, UUID> {

    private List<Ticket> tickets;
    private JacksonReadWrite<Ticket> jacksonReadWrite;

    public JsonTicketRepositoryImpl() throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>("Tickets.json");
        tickets = jacksonReadWrite.read(Ticket[].class);
    }

    public JsonTicketRepositoryImpl(String fileName) throws IOException {
        jacksonReadWrite = new JacksonReadWrite<>(fileName);
        tickets = jacksonReadWrite.read(Ticket[].class);
    }

    @Override
    public List<Ticket> getAll() {
        return tickets.stream()
                .map(Ticket::new)
                .sorted(Comparator.comparing(Ticket::getDate))
                .collect(Collectors.toList());
    }

    @Override
    public Ticket getById(UUID id) {
        return tickets.stream()
                .filter(t -> t.getId().equals(id))
                .findFirst()
                .map(Ticket::new)
                .orElseThrow(() -> new IllegalArgumentException("Ticket with this id doesn't exist"));
    }

    @Override
    public boolean isExistsById(UUID id) {
        return tickets.stream()
                .anyMatch(t -> t.getId().equals(id));
    }

    @Override
    public void checkExistsById(UUID id) throws IllegalArgumentException {
        if(!isExistsById(id))
            throw new IllegalArgumentException("Ticket with this id doesn't exist");
    }

    @Override
    public boolean isContains(Ticket ticket) throws IllegalArgumentException {
        return tickets.stream()
                .anyMatch(u -> u.equals(ticket));
    }

    @Override
    public void checkNotContains(Ticket ticket) throws IllegalArgumentException {
        if(isContains(ticket))
            throw new IllegalArgumentException("Ticket with these settings already exists");
    }

    @Override
    public void deleteAll() {
        tickets = new ArrayList<>();
    }

    @Override
    public void create(Ticket ticket) {
        tickets.add(ticket);
    }

    @Override
    public void update(Ticket newTicket) {
        Ticket oldTicket = getById(newTicket.getId());
        tickets.set(tickets.indexOf(oldTicket), newTicket);
    }

    @Override
    public void deleteById(UUID id) {
        Ticket ticket = getById(id);
        tickets.remove(ticket);
    }

    @Override
    public void save() throws IOException {
        jacksonReadWrite.write(tickets);
    }

    @Override
    public void deleteByUser(User user) {
        List<Ticket> deleteTickets = tickets.stream()
                .filter(t -> t.getUser().equals(user))
                .collect(Collectors.toList());
        tickets.removeAll(deleteTickets);
    }

    @Override
    public void deleteByMovie(Movie movie) {
        List<Ticket> deleteTickets = tickets.stream()
                .filter(t -> t.getMovie().equals(movie))
                .collect(Collectors.toList());
        tickets.removeAll(deleteTickets);
    }

    @Override
    public List<User> getUsersByDateOfDay(LocalDateTime date) {
        return tickets.stream()
                .filter(t -> ChronoUnit.DAYS.between(date, t.getDate()) == 0)
                .map(Ticket::getUser)
                .distinct()
                .sorted(Comparator.comparing(User::getName))
                .collect(Collectors.toList());
    }

    @Override
    public List<Movie> getMoviesByCurrentDateOfDay() {
        LocalDateTime date = LocalDateTime.now();
        return tickets.stream()
                .filter(t -> date.getYear() == t.getDate().getYear() && date.getMonth() == t.getDate().getMonth() && t.getDate().getDayOfYear() == date.getDayOfYear())
                .map(Ticket::getMovie)
                .sorted(Comparator.comparing(Movie::getTitle))
                .collect(Collectors.toList());
    }

    @Override
    public float getSumByDateOfMonth(LocalDateTime date) {
        return (float) tickets.stream()
                .filter(t -> date.getYear() == t.getDate().getYear() && date.getMonth() == t.getDate().getMonth())
                .mapToDouble(Ticket::getPrice)
                .sum();
    }

    @Override
    public List<Movie> getSortedMoviesByCountOfTickets() {
        Map<Movie, Long> countOfEachMovie = tickets.stream()
                .collect(Collectors.groupingBy(Ticket::getMovie, Collectors.counting()));

        List<Movie> sortedMovies = countOfEachMovie.entrySet()
                .stream()
                .sorted(Map.Entry.<Movie, Long>comparingByValue()
                        .thenComparing(obj -> obj.getKey().getTitle()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return sortedMovies;
    }

    @Override
    public List<Movie> getMoviesIfCountOfTicketsByMovieLessThenNumber(long number) {
        Map<Movie, Long> countOfEachMovie = tickets.stream()
                .map(Ticket::getMovie)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<Movie> foundMovies = countOfEachMovie.entrySet()
                .stream()
                .filter(entry -> entry.getValue() < number)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return foundMovies;
    }

}
