package cinema.db_util;

import lombok.Setter;
import lombok.SneakyThrows;

import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbHandler {
    private Connection connection;
    private static DbHandler dbHandler;

    public Connection getConnection() {
        return connection;
    }

    @SneakyThrows
    private DbHandler() throws SQLException {
        Properties props = new Properties();
        String dbSettingsPropertyFile = "src\\main\\resources\\jdbc.properties";
        try(FileReader fReader = new FileReader(dbSettingsPropertyFile)) {
            props.load(fReader);
            String dbConnUrl = props.getProperty("db.conn.url");
            String dbUserName = props.getProperty("db.username");
            String dbPassword = props.getProperty("db.password");
            connection = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);
        }
        connection.setAutoCommit(false);
    }

    public static Connection getInstanceConnection() throws SQLException {
        if(dbHandler == null)
            dbHandler = new DbHandler();
        return dbHandler.getConnection();
    }
}
