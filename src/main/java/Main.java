import cinema.entities.*;
import cinema.exceptions.DataException;
import cinema.repositories.db_repositories_impl.DbMovieRepositoryImpl;
import cinema.repositories.db_repositories_impl.DbTicketRepositoryImpl;
import cinema.repositories.db_repositories_impl.DbUserRepositoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        new DataException(new RuntimeException("my error"));
//        DbUserRepositoryImpl dbUserRepository = new DbUserRepositoryImpl();
//        DbUserRepositoryImpl dbUserRepository1 = new DbUserRepositoryImpl();
//        DbMovieRepositoryImpl dbMovieRepository = new DbMovieRepositoryImpl();
//        DbTicketRepositoryImpl dbTicketRepository = new DbTicketRepositoryImpl();

//        dbUserRepository1.deleteAll();
//        for(User user : dbUserRepository1.getAll()){
//            System.out.println(user);
//        }
//        //==========================================Ticket_BD

//        Ticket ticket1 = new TicketBuilder()
//                .setUser(dbUserRepository.getById(UUID.fromString("e587921b-4e50-4428-8194-e2a7a4198e60")))
//                .setMovie(dbMovieRepository.getById(UUID.fromString("7dde7f30-34ba-46ef-a981-e6d2b7668be4")))
//                .setDate(LocalDateTime.of(2021, 5, 11, 4,33, 0))
//                .setPrice(26.22f)
//                .build();
//        dbTicketRepository.create(ticket1);
//        Ticket ticketUpdate = dbTicketRepository.getById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130033"));
//        ticketUpdate.setPrice(999.99f);
//        dbTicketRepository.update(ticketUpdate);
//        dbTicketRepository.deleteAll();
//
//        System.out.println("-print all");
//        for(Ticket ticket : dbTicketRepository.getAll()) {
//            System.out.println(ticket);
//        }
//        System.out.println("-getUsersByDateOfDay");
//        for(User user : dbTicketRepository.getUsersByDateOfDay(LocalDateTime.of(2021, 5, 11, 10, 5,6))) {
//            System.out.println(user);
//        }
//        System.out.println("-getMoviesByCurrentDateOfDay");
//        for(Movie movie : dbTicketRepository.getMoviesByCurrentDateOfDay()) {
//            System.out.println(movie);
//        }
//        System.out.println("-getSortedMoviesByCountOfTickets");
//        for(Movie movie : dbTicketRepository.getSortedMoviesByCountOfTickets()) {
//            System.out.println(movie);
//        }
//        System.out.println("-getMoviesIfCountOfTicketsByMovieLessThenNumber");
//        for(Movie movie : dbTicketRepository.getMoviesIfCountOfTicketsByMovieLessThenNumber(2)) {
//            System.out.println(movie);
//        }
//        System.out.println("-getSumByDateOfMonth");
//        System.out.println(dbTicketRepository.getSumByDateOfMonth(LocalDateTime.of(2021, 6, 11, 10, 5,6)));

//        dbTicketRepository.save();
//        System.out.println(dbTicketRepository.isContains(ticket1));
//        System.out.println(dbTicketRepository.getById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130033")));
//        //==========================================Ticket_BD


//        //==========================================Movie_BD
//        Movie movie1 = new MovieBuilder()
//                .setTitle("movie1")
//                .setDuration(Duration.ofMinutes(34))
//                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
//                .setRating(1.1f)
//                .build();
//
//        dbMovieRepository.create(movie1);
//        dbMovieRepository.save();
//        dbMovieRepository.deleteById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130013"));
//
//        Movie movieUpdate = dbMovieRepository.getById(UUID.fromString("7dde7f30-34ba-46ef-a981-e6d2b7668be4"));
//        movieUpdate.setDuration(Duration.ofMinutes(111));
//        movieUpdate.setTitle("12345");
//        movieUpdate.setGenres(Set.of(Genre.FANTASY));
//        movieUpdate.setRating(1.1f);
//        dbMovieRepository.update(movieUpdate);
//
//        for(Movie movie : dbMovieRepository.getAll()) {
//            System.out.println(movie);
//        }
//        System.out.println(dbMovieRepository.isExistsById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130013")));
//        System.out.println(dbMovieRepository.isContains(movie1));
//        //==========================================Movie_BD

//        //==========================================USER_BD
//        User user2 = new UserBuilder()
//                .setName("person")
//                .setPhone("+38 (095) 23-12-477")
//                .build();
//        dbUserRepository.create(user2);
//        dbUserRepository.deleteById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130003"));
//        dbUserRepository.deleteAll();
//        User user = dbUserRepository.getById(UUID.fromString("34fc854a-d4d8-11eb-b8bc-0242ac130003"));
//        user.setName("new name");
//        user.setPhone("098396105");
//        dbUserRepository.update(user);
//        dbUserRepository.isExistsById(UUID.fromString("9a76346a-9d90-43a2-a9c9-6ab41e14d2e9"));
//        for(User u : dbUserRepository.getAll()) {
//            System.out.println(u);
//        }
//        dbUserRepository.save();
//        //==========================================USER_BD


//        MovieService movieService = new MovieService();
//        UserService userService = new UserService();
//        TicketService ticketService = new TicketService();
//
//        //==========================================Ticket
//
//        Ticket ticket1 = new TicketBuilder()
//                .setUser(userService.getById(UUID.fromString("aa04a967-e6f1-4b49-a45c-2278668b04a5")))
//                .setMovie(movieService.getById(UUID.fromString("c726af0a-5a75-4ce6-8d7d-6cd2600f698c")))
//                .setDate(LocalDateTime.of(2021, 6, 21, 8,44, 0))
//                .setPrice(101.1f)
//                .build();
//        Ticket ticket2 = new TicketBuilder()
//                .setUser(userService.getById(UUID.fromString("aa04a967-e6f1-4b49-a45c-2278668b04a5")))
//                .setMovie(movieService.getById(UUID.fromString("7bb250de-3f83-4d07-b9cf-c8c01b32befb")))
//                .setPrice(321.6f)
//                .setDate(LocalDateTime.of(2021, 5, 11, 11,44, 0))
//                .build();
//        Ticket ticket3 = new TicketBuilder()
//                .setUser(userService.getById(UUID.fromString("0adab271-9597-4b3e-b1df-ee3408d6400f")))
//                .setMovie(movieService.getById(UUID.fromString("0d51d2c9-ce61-468a-85c5-ef05690f2241")))
//                .setPrice(156.1f)
//                .build();
//        Ticket ticket4 = new TicketBuilder()
//                .setUser(userService.getById(UUID.fromString("3ab0635f-56b7-41a6-88f2-0951460aeed5")))
//                .setMovie(movieService.getById(UUID.fromString("9a94190c-4db1-4c82-96a4-0221297995ca")))
//                .setPrice(123.6f)
//                .setDate(LocalDateTime.of(2021, 5, 11, 17,33, 0))
//                .build();
////        ticketService.create(ticket1);
////        ticketService.create(ticket2);
////        ticketService.create(ticket3);
////        ticketService.create(ticket4);
//
//        System.out.println("-ALL MOVIES BY CURRENT DATE");
//        for(Movie m : ticketService.getMoviesByCurrentDateOfDay()) {
//            System.out.println(m);
//        }
//
//        LocalDateTime ldt =  LocalDateTime.of(2021, 5, 11, 17,33, 0);
//        System.out.println("-ALL USERS BY DATE");
//        for(User t : ticketService.getUsersByDateOfDay(ldt)) {
//            System.out.println(t);
//        }
//
//        System.out.println("-SUM PRICE OF MONTH");
//        System.out.println(ticketService.getSumByDateOfMonth(ldt));
//
//        System.out.println("-SORTED MOVIES BY COUNT OF TICKETS");
//        for(Movie m : ticketService.getSortedMoviesByCountOfTickets()) {
//            System.out.println(m);
//        }
//        System.out.println("-MOVIES THAT COUNT OF TICKETS LESS THEN NUMBER");
//        for(Movie m : ticketService.getMoviesIfCountOfTicketsByMovieLessThenNumber(2)) {
//            System.out.println(m);
//        }
//
//
//        ticketService.save();
//        //==========================================Ticket
//
//        //==========================================USER
//
//        User user1 = new UserBuilder()
//                .setName("vadim")
//                .setPhone("+38 (095) 88-27-299")
//                .build();
//        User user2 = new UserBuilder()
//                .setName("person")
//                .setPhone("+38 (095) 23-12-477")
//                .build();
//        User user3 = new UserBuilder()
//                .setName("new person")
//                .setPhone("+38 (095) 12-34-567")
//                .build();
//        User user4 = new UserBuilder()
//                .setName("person1")
//                .setPhone("+38 (095) 12-56-124")
//                .build();
//        User user5 = new UserBuilder()
//                .setName("person2")
//                .setPhone("+38 (098) 99-88-444")
//                .build();
//        User user6 = new UserBuilder()
//                .setName("person3")
//                .setPhone("+38 (097) 12-66-842")
//                .build();
////        userService.create(user1);
////        userService.create(user2);
////        userService.create(user3);
////        userService.create(user4);
////        userService.create(user5);
////        userService.create(user6);
////        User user = userService.getAll().get(0);
////        user.setName("new Name3456");
////        userService.deleteById(user.getId());
//
//        userService.save();
//        //==========================================USER
//
//        //==========================================Movie
//
//        Movie movie1 = new MovieBuilder()
//                .setTitle("movie1")
//                .setDuration(Duration.ofMinutes(34))
//                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
//                .setRating(1.1f)
//                .build();
//        Movie movie2 = new MovieBuilder()
//                .setTitle("movie2")
//                .setDuration(Duration.ofMinutes(56))
//                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
//                .setRating(1.1f)
//                .build();
//        Movie movie3 = new MovieBuilder()
//                .setTitle("movie3")
//                .setDuration(Duration.ofMinutes(11))
//                .setGenres(Set.of(Genre.HORROR, Genre.COMEDY))
//                .setRating(2f)
//                .build();
//        Movie movie4 = new MovieBuilder()
//                .setTitle("movie4")
//                .setDuration(Duration.ofMinutes(125))
//                .setGenres(Set.of(Genre.FANTASY, Genre.HORROR))
//                .setRating(6.6f)
//                .build();
//        Movie movie5 = new MovieBuilder()
//                .setTitle("movie5")
//                .setDuration(Duration.ofMinutes(64))
//                .setGenres(Set.of(Genre.FANTASY, Genre.DRAMA))
//                .setRating(5.5f)
//                .build();
//        Movie movie6 = new MovieBuilder()
//                .setTitle("movie6")
//                .setDuration(Duration.ofMinutes(45))
//                .setGenres(Set.of(Genre.HORROR))
//                .setRating(7.7f)
//                .build();
//
////        movieService.create(movie1);
////        movieService.create(movie2);
////        movieService.create(movie3);
////        movieService.create(movie4);
////        movieService.create(movie5);
////        movieService.create(movie6);
//
//        movieService.save();
//        //==========================================Movie
//
//
//
//        System.out.println("TICKETS");
//        for(Ticket t : ticketService.getAll()) {
//            System.out.println(t);
//        }
//
//        System.out.println("MOVIES");
//        for(Movie m : movieService.getAll()) {
//            System.out.println(m);
//        }
//
//        System.out.println("USERS");
//        for(User u : userService.getAll()) {
//            System.out.println(u);
//        }
    }
}
